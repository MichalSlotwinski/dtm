package project.micslo.machine;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import project.micslo.machine.config.MachineSeven;
import project.micslo.machine.state.Neighbour;
import project.micslo.machine.state.Node;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class MachineBuilderTest
{
    private MachineSeven seven;

    @Before
    public void setUp()
    {
        try
        {
            this.seven = MachineSeven.load(new File("src/test/resources/example.configuration"));
        }
        catch(FileNotFoundException e) {e.printStackTrace();}
        catch(IOException e) {e.printStackTrace();}
    }

    @Test
    public void machineTest()
    {
        Machine machine = MachineBuilder.build(this.seven);

        Node curr = machine.getEntry();
        List<Neighbour> neighbours = curr.getNeighbours();

        Assert.assertEquals('a', neighbours.get(0).getRead().charValue());
        Assert.assertEquals(2, neighbours.get(0).getState().getLabel());
        Assert.assertEquals('a', neighbours.get(0).getWrite().charValue());
        Assert.assertEquals('P', neighbours.get(0).getMove().charValue());

        Assert.assertEquals('b', neighbours.get(1).getRead().charValue());
        Assert.assertEquals(2, neighbours.get(1).getState().getLabel());
        Assert.assertEquals('#', neighbours.get(1).getWrite().charValue());
        Assert.assertEquals('P', neighbours.get(1).getMove().charValue());

        curr = neighbours.get(0).getState();
        neighbours = curr.getNeighbours();

        Assert.assertEquals('a', neighbours.get(0).getRead().charValue());
        Assert.assertEquals(1, neighbours.get(0).getState().getLabel());
        Assert.assertEquals('a', neighbours.get(0).getWrite().charValue());
        Assert.assertEquals('L', neighbours.get(0).getMove().charValue());

        Assert.assertEquals('b', neighbours.get(1).getRead().charValue());
        Assert.assertEquals(2, neighbours.get(1).getState().getLabel());
        Assert.assertEquals('b', neighbours.get(1).getWrite().charValue());
        Assert.assertEquals('P', neighbours.get(1).getMove().charValue());

        Assert.assertEquals('#', neighbours.get(2).getRead().charValue());
        Assert.assertEquals(3, neighbours.get(2).getState().getLabel());
        Assert.assertEquals('#', neighbours.get(2).getWrite().charValue());
        Assert.assertEquals('L', neighbours.get(2).getMove().charValue());

        curr = neighbours.get(2).getState();
        neighbours = curr.getNeighbours();

        Assert.assertEquals('#', neighbours.get(0).getRead().charValue());
        Assert.assertEquals(3, neighbours.get(0).getState().getLabel());
        Assert.assertEquals('#', neighbours.get(0).getWrite().charValue());
        Assert.assertEquals('L', neighbours.get(0).getMove().charValue());
    }
}
