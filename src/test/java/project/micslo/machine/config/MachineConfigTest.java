package project.micslo.machine.config;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import project.micslo.utils.Relation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MachineConfigTest
{
    private MachineSeven config;

    @Before
    public void setUp()
    {
        config = new MachineSeven();
    }

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void machineConfigFileNotExistTest()
    {
        File path = new File("/non/existent/path/to/file.configuration");
        try
        {
            MachineSeven.load(path);
            Assert.fail();
        }
        catch(FileNotFoundException e)
        { Assert.assertEquals("File: " + path.toPath() + " doesn't exist.", e.getMessage()); }
        catch(IOException e) {}
    }

    @Test
    public void machineConfigFileIsDirectoryExceptionTest()
    {
        File path = new File("src/test/resources/");
        try
        {
            MachineSeven.load(path);
            Assert.fail();
        }
        catch(FileNotFoundException e)
        { Assert.assertEquals("Path: " + path.toPath() + " is a directory instead of file.", e.getMessage()); }
        catch(IOException e) {}
    }

    @Test
    public void machineConfigTest()
    {
        try
        {
            List<Character> l = new ArrayList<>();
            File file = new File("src/test/resources/example.configuration");
            this.config = MachineSeven.load(file);
            Assert.assertEquals(this.config.getAlphabetTape().size(), 3);
            l.add('a');l.add('b');l.add('#');
            Assert.assertEquals(this.config.getAlphabetTape(), l);
            l.clear();

            Assert.assertEquals(this.config.getAlphabetInput().size(), 2);
            l.add('a');l.add('b');
            Assert.assertEquals(this.config.getAlphabetInput(), l);
            l.clear();

            Assert.assertEquals(this.config.getWordInput().size(), 3);
            l.add('b');l.add('b');l.add('b');
            Assert.assertEquals(this.config.getWordInput(), l);
            l.clear();

            List<Integer> t = new ArrayList<>();
            Assert.assertEquals(this.config.getStates().size(), 3);
            t.add(1);t.add(2);t.add(3);
            Assert.assertEquals(this.config.getStates(), t);
            t.clear();

            Assert.assertEquals(this.config.getBeginState().intValue(), 1);
            Assert.assertEquals(this.config.getBeginState().intValue(), 1);

            Assert.assertEquals(this.config.getAcceptStates().size(), 1);
            t.add(3);
            Assert.assertEquals(this.config.getAcceptStates(), t);
            t.clear();

            List<Relation> r = new ArrayList<>();
            Assert.assertEquals(this.config.getTransitions().size(), 6);
            r.add(new Relation(1, 'a', 2, 'a', 'P'));r.add(new Relation(1, 'b', 2, '#', 'P'));
            r.add(new Relation(2, 'a', 1, 'a', 'L'));r.add(new Relation(2, 'b', 2, 'b', 'P'));
            r.add(new Relation(2, '#', 3, '#', 'L'));r.add(new Relation(3, '#', 3, '#', 'L'));
            for(int i = 0; i < r.size(); ++i)
            {
                Assert.assertEquals(r.get(i).from(), this.config.getTransitions().get(i).from());
                Assert.assertEquals(r.get(i).read(), this.config.getTransitions().get(i).read());
                Assert.assertEquals(r.get(i).to(), this.config.getTransitions().get(i).to());
                Assert.assertEquals(r.get(i).write(), this.config.getTransitions().get(i).write());
                Assert.assertEquals(r.get(i).move(), this.config.getTransitions().get(i).move());
            }
            r.clear();
        }
        catch (Exception e) {Assert.fail(e.getClass().getName());}
    }

    @Test
    public void machineConfigStatesParse()
    {
        List<Integer> states = new ArrayList<>();
        states.add(1);states.add(2);states.add(3);states.add(4);states.add(5);states.add(6);states.add(7);
        states.add(8);states.add(9);states.add(10);states.add(11);states.add(12);states.add(13);states.add(14);
        List<Integer> accept = new ArrayList<>();
        accept.add(5);accept.add(11);accept.add(14);
        File file = new File("src/test/resources/unary_sum.configuration");
        try
        {
            this.config = MachineSeven.load(file);
            Assert.assertEquals(states, this.config.getStates());
            Assert.assertEquals(accept, this.config.getAcceptStates());
        }
        catch(IOException e) { e.printStackTrace(); }
    }
}
