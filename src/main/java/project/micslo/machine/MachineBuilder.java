package project.micslo.machine;

import project.micslo.UI.logger.Logger;
import project.micslo.machine.config.MachineSeven;
import project.micslo.machine.state.Neighbour;
import project.micslo.machine.state.Node;
import project.micslo.utils.Relation;
import project.micslo.utils.Tape;

import java.util.ArrayList;
import java.util.List;

/**
 * Klasa budujaca maszyne z formalnej konfiguracji.
 */
public class MachineBuilder
{
    /**
     * Buduje maszyne. Proces budoowy podzielony jest na 2 etapy:
     * <ul>
     *     <li>budowania stanow i relacji miedzy nimi</li>
     *     <li>budowania tasmy</li>
     * </ul>
     * @return
     */
    public static Machine build(MachineSeven seven, Logger logger)
    {
        Machine machine = new Machine();
        MachineBuilder.buildStates(seven, machine);
        MachineBuilder.buildTape(seven, machine, logger);
        logger.nl().append("[INFO] Build successful!");
        return machine;
    }

    /**
     * Buduje maszyne. Proces budoowy podzielony jest na 2 etapy:
     * <ul>
     *     <li>budowania stanow i relacji miedzy nimi</li>
     *     <li>budowania tasmy</li>
     * </ul>
     * @return
     */
    public static Machine build(MachineSeven seven)
    {
        Machine machine = new Machine();
        MachineBuilder.buildStates(seven, machine);
        MachineBuilder.buildTape(seven, machine, null);
        return machine;
    }

    /**
     * Etap budowania stanow.
     * Najpierw tworzy stany i okrasla czy sa wejsciowy, czy akceptujace, a nastepnie dokonuje powiazania stanow ze soba.
     */
    private static void buildStates(MachineSeven seven, Machine machine)
    {
        List<Node> nodes = new ArrayList<>();
        Node curr = null;

        // Tworzenie stanow i okreslanie czy wejsciowy, czy akceptujacy.
        for(Integer stateLabel : seven.getStates())
        {
            curr = new Node(stateLabel);
            nodes.add(curr);

            if(seven.getAcceptStates().contains(stateLabel))
                curr.setAccept(true);
            else
                curr.setAccept(false);

            if(stateLabel.equals(seven.getBeginState()))
            {
                curr.setEntry(true);
                machine.setCurrentNode(curr);
            }
            else
                curr.setEntry(false);
        }

        // Powiazywanie stanow
        Node neighbour = null;
        for(Relation relation : seven.getTransitions())
        {
            curr = nodes.get(relation.from()-1);
            neighbour = nodes.get(relation.to()-1);
            curr.addNeighbour(new Neighbour(neighbour, relation.read(), relation.write(), relation.move()));
        }
    }

    /**
     * Etap budowania tasmy.
     */
    private static void buildTape(MachineSeven seven, Machine machine, Logger logger)
    {
        Tape tape = new Tape(seven.getWordInput(), logger);
        machine.setTape(tape);
    }
}
