package project.micslo.machine;

import project.micslo.UI.logger.Logger;
import project.micslo.machine.state.Neighbour;
import project.micslo.machine.state.Node;
import project.micslo.utils.Direction;
import project.micslo.utils.Tape;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * Klasa dokonujaca obliczenia na maszynie.
 */
public class MachineRunner
{
    private Machine machineTuring;
    private Logger logger;
    private boolean print;
    private long computationLength;
    private String startWord;

    public MachineRunner(Machine machineTuring)
    {
        this.startWord = machineTuring.getWord();
        this.machineTuring = machineTuring;
        this.logger = null;
    }

    public MachineRunner(Machine machineTuring, Logger logger)
    {
        this.machineTuring = machineTuring;
        this.startWord = machineTuring.getWord();
        this.logger = logger;
    }

    /**
     * Wykonuje kolejne iteracje maszyny Turinga.
     * @param consumer co ma się dziać między iteracjami
     * @return liczba wykonanych iteracji
     * @throws Exception bo może teoretycznie
     */
    public long run(Consumer<Machine> consumer, int iterations) throws Exception
    {
        Node node = machineTuring.getEntry();
        int i = 0;
        while (node != null && !node.isAccept() && i < iterations)
        {
            consumer.accept(machineTuring);
            node = iterate(node).orElse(null);
            if (node != null)
            {
                machineTuring.setCurrentNode(node);
                i++;
            }
        }
        if (node != null)
        {
            consumer.accept(machineTuring);
            if (node.isAccept()){
                if(this.logger != null)
                {
                    if(!this.print)
                    {
                        this.logger.nl().nl().append("Accepting state found.");
                    }
                    else
                    {
                        this.logger.nl().append("Accepting state found.");
                        this.logger.nl().append("Start word: " + startWord);
                        this.logger.nl().append("Result word: " + machineTuring.getWord());
                    }
                }
            }
        }
        else {
            if(this.logger != null)
            {
                if(!this.print)
                {
                    this.logger.nl().nl().append("Machine hanged.");
                }
                else
                {
                    this.logger.nl().append("Machine hanged.");
                }
            }
        }
        this.computationLength += i;
        return this.computationLength;
    }

    public Optional<Node> iterate(Node currentNode) throws Exception
    {
        List<Neighbour> list = currentNode.getNeighbours();
        Tape tape = machineTuring.getTape();
        char c = tape.getCharUnderHead();
        Node next = null;
        for (Neighbour n: list)
        {
            if (n.getRead().equals(c))
            {
                next = n.getState();
                tape.setCharAt(n.getWrite(), tape.getHeadPos());
                tape.move(Direction.fromCharacter(n.getMove()).orElse(null));
            }
        }
        return Optional.ofNullable(next);
    }

    public void setPrint() { this.print = true; }
    public void setDoNotPrint() { this.print = false; }
}
