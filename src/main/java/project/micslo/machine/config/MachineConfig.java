package project.micslo.machine.config;

/**
 * Klasa reprezentujaca konfiguracje maszyny w momencie czasu.
 */
public class MachineConfig
{
    private Integer state;
    private String prefix;
    private String remainingWord;

    public MachineConfig(Integer state, String prefix, String remainingWord)
    {
        this.state = state;
        this.prefix = prefix;
        this.remainingWord = remainingWord;
    }

    public Integer getState()
    {
        return state;
    }

    public String getPrefix()
    {
        return prefix;
    }

    public String getRemainingWord()
    {
        return remainingWord;
    }
}
