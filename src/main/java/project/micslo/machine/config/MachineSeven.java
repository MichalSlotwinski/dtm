package project.micslo.machine.config;

import project.micslo.utils.Relation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Klasa bedaca reprezentacja formalnej definicji maszyny:
 * <ol>
 *     <li>𝛤 - alfabet tasmowy</li>
 *     <li>𝛴 ⊂ 𝛤 - alfabet wejsciowy</li>
 *     <li># ∈ 𝛤 \ 𝛴 - symbol pusty</li>
 *     <li>Q - zbior stanow</li>
 *     <li>s ∈ Q - stan poczatkowy</li>
 *     <li>F ⊂ Q - zbior stanow akceptujacych</li>
 *     <li>𝛿 ⊂ (Q x 𝛤) x (Q x 𝛤 x {L, P}) - relacja przejscia: Q x 𝛤 -> Q x 𝛤 x {L, P} jest funkcja czesciowa</li>
 * </ol>
 */
public class MachineSeven
{
    List<Character> alphabetTape;
    List<Character> alphabetInput;
    List<Character> wordInput;
    List<Integer> states;
    Integer beginState;
    List<Integer> acceptStates;
    List<Relation> transitions;

    public MachineSeven()
    {
        this.alphabetTape = null;
        this.alphabetInput = null;
        this.wordInput = null;
        this.states = null;
        this.beginState = null;
        this.acceptStates = null;
        this.transitions = new ArrayList<>();
    }

    /**
     * Wczytuje konfiguracje maszyny z pliku.
     * @param file Sciezka do pliku
     * @throws FileNotFoundException Jesli plik nie istnieje lub jest katalogiem zamiast plikiem.
     * @throws IOException Jesli wystapily problemy z wczytaniem pliku.
     * @return Zainicjalizowana siódemka maszyny.
     */
    public static MachineSeven load(File file) throws FileNotFoundException, IOException
    {
        if(!file.exists())
        {
            throw new FileNotFoundException("File: " + file.toPath() + " doesn't exist.");
        }
        if(file.isDirectory())
        {
            throw new FileNotFoundException("Path: " + file.toPath() + " is a directory instead of file.");
        }

        MachineSeven seven = new MachineSeven();
        ConfigParser parser = new ConfigParser(seven);
        Stream<String> lines = Files.lines(file.toPath());
        lines.forEach(parser::accept);
        lines.close();
        return seven;
    }

    public List<Character> getAlphabetTape()
    {
        return alphabetTape;
    }

    public List<Character> getAlphabetInput()
    {
        return alphabetInput;
    }

    public List<Character> getWordInput()
    {
        return wordInput;
    }

    public List<Integer> getStates()
    {
        return states;
    }

    public Integer getBeginState()
    {
        return beginState;
    }

    public List<Integer> getAcceptStates()
    {
        return acceptStates;
    }

    public List<Relation> getTransitions()
    {
        return transitions;
    }
}
