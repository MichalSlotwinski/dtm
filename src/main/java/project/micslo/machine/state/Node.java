package project.micslo.machine.state;

import java.util.ArrayList;
import java.util.List;

/**
 * Klasa reprezentujaca pojedynczy stan w maszynie.
 */
public class Node
{
    private int label;
    private boolean entry;
    private boolean accept;
    private List<Neighbour> neighbours;

    public Node(int label)
    {
        this.label = label;
        this.neighbours = new ArrayList<>();
    }

    public void setEntry(boolean entry) { this.entry = entry; }

    public void setAccept(boolean accept) { this.accept = accept; }

    public void addNeighbour(Neighbour neighbour) { this.neighbours.add(neighbour); }

    public int getLabel() { return label; }

    public boolean isEntry() { return entry; }

    public boolean isAccept() { return accept; }

    public List<Neighbour> getNeighbours() { return neighbours; }
}
