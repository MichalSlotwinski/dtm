package project.micslo.machine;

import project.micslo.machine.state.Node;
import project.micslo.utils.Tape;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Klasa bedaca reprezentacja maszyny.
 */
public class Machine
{
    private Node node;
    private Tape tape;

    public Machine()
    {
        this.node = null;
        this.tape = null;
    }

    public void setCurrentNode(Node node) { this.node = node; }

    public void setTape(Tape tape) { this.tape = tape; }

    public Tape getTape() { return tape; }

    public Node getEntry() { return this.node; }

    public String getWord()
    {
        return tape.getTapeContent().stream().filter(ch -> ch != '#')
                .map(String::valueOf).collect(Collectors.joining());
    }

    @Override
    public String toString()
    {
        return (node.getLabel() + ": " + tape.getTapeContent());
    }
}
