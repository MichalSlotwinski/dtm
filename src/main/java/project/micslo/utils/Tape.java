package project.micslo.utils;

import project.micslo.UI.logger.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Klasa bedaca reprezentacja tasmy maszyny.
 */
public class Tape
{
    public static final char _EMPTY = '#';
    public static final int _SIZE = 32;
    protected List<Character> tape;
    private int headPos = 1;
    private Logger logger;

    public Tape(List<Character> word, Logger logger)
    {
        this.logger = logger;
        init();
        writeWord(word);
    }

    /**
     * Inicjalizuje tasme znakami pustymi o dlugosci _SIZE.
     */
    public void init()
    {
        this.tape = Stream.generate(() -> _EMPTY).limit(_SIZE).collect(Collectors.toList());
    }

    /**
     * Pisze slowo do pustej tasmy.
     * @param word Slowo do wstawienia do pustej tasmy.
     */
    public void writeWord(List<Character> word)
    {
        this.tape.addAll(this.headPos, word);
        int scale = (int)Math.floor(this.tape.size() / (float)_SIZE);
        this.tape = this.tape.subList(0, scale * _SIZE);
    }

    /**
     * Rozszerza tasme w tyl, w przypadku gdy zajdzie potrzeba wielokrotnego cofania sie.
     */
    public void extendBack()
    {
        this.tape.addAll(0, Stream.generate(() -> _EMPTY).limit(_SIZE).collect(Collectors.toList()));
    }

    /**
     * Rozszerza tasme w przod, w przypadku gdy zajdzie potrzeba wielokrotnego zajmowania kolejnych komorek.
     */
    public void extendFront()
    {
        this.tape.addAll(Stream.generate(() -> _EMPTY).limit(_SIZE).collect(Collectors.toList()));
    }

    /**
     * Wykonuje ruch na tasmie:
     * <ul>
     *     <li>L- lewo</li>
     *     <li>P - prawo</li>
     * </ul>
     * @param direction Kierunek ruchu: LEFT lub RIGHT.
     * @throws Exception W przypadku podania opcji innej niz LEFT lub RIGHT.
     */
    public void move(Direction direction) throws Exception
    {
        if (direction == null)
        {
            throw new Exception("[ERROR] Invalid option! Available options: 'RIGHT' and 'LEFT'.");
        }
        switch(direction)
        {
            case RIGHT:
                if(this.headPos + 1 > this.tape.size() - 1)
                {
                    extendFront();
                    if (logger != null)
                    {
                        logger.nl().append("[INFO] Tape extended to the right!");
                    }
                }
                this.headPos++;
                break;
            case LEFT:
                if(this.headPos - 1 < 0)
                {
                    extendBack();
                    this.headPos += _SIZE;
                    if (logger != null)
                    {
                        logger.nl().append("[INFO] Tape extended to the left!");
                    }
                }
                this.headPos--;
                break;
        }

    }

    public Character getCharUnderHead() { return this.tape.get(this.headPos); }

    public Collection<Character> getTapeContent() { return new ArrayList<>(tape); }

    public int getHeadPos() { return headPos; }

    public void setCharAt(char character, int index) { tape.set(index, character); }

}
