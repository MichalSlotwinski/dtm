package project.micslo.utils;

import java.util.Optional;

public enum Direction
{
    LEFT,
    RIGHT;

    public static Optional<Direction> fromCharacter(char character)
    {
        Optional<Direction> direction = Optional.empty();
        switch (Character.toUpperCase(character))
        {
            case 'P':
                direction = Optional.of(RIGHT);
                break;
            case 'L':
                direction = Optional.of(LEFT);
                break;
        }
        return direction;
    }
}
