package project.micslo.utils;

/**
 * Relacja przejscia
 */
public class Relation
{
    private Integer from;
    private Character read;
    private Integer to;
    private Character write;
    private Character move;

    public Relation(Integer from, Character read, Integer to, Character write, Character move)
    {
        this.from = from;
        this.read = read;
        this.to = to;
        this.write = write;
        this.move = move;
    }

    /**
     * @return Stan, z ktorego wychodzi relacja.
     */
    public Integer from() { return from; }
    public Integer getFrom() { return from; }

    /**
     * @return Znak pod glowica.
     */
    public Character read() { return read; }
    public Character getRead() { return read; }

    /**
     * @return Stan, do ktorego wchodzi relacja.
     */
    public Integer to() { return to; }
    public Integer getTo() { return to; }

    /**
     * @return Znak pisany do komorki pod glowica.
     */
    public Character write() { return write; }
    public Character getWrite() { return write; }

    /**
     * @return Kierunek ruchu tasmy.
     */
    public Character move() { return move; }
    public Character getMove() { return move; }
}
