package project.micslo;


import project.micslo.UI.Controller;

/**
 * Klasa opakowujaca klase Controller.
 */
public class DTM
{
    public static void main(String[] args)
    {
        Controller controller = new Controller();
        controller.run(args);
    }
}
