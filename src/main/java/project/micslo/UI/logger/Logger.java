package project.micslo.UI.logger;

import javafx.application.Platform;
import org.fxmisc.richtext.CodeArea;


public class Logger
{
    private CodeArea terminal;

    public Logger(CodeArea terminal)
    {
        this.terminal = terminal;
    }

    public Logger append(String text)
    {
        Platform.runLater(() -> {
                terminal.appendText(text);
                terminal.requestFollowCaret();
        });

        return this;
    }

    public Logger nl()
    {
        Platform.runLater(() -> {
                terminal.appendText("\n");
                terminal.requestFollowCaret();
        });

        return this;
    }
}
