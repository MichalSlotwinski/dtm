# Build
- mvn package

# Tasks
- Przemek:
    - [x] Machine step logic
    - [x] Std output (text)
- Michał:
    - [x] Graphical visualisation (graph with transitions) (UI)
    - [x] Std bind to UI
- Anybody:
    - [x] Bugs
    - [x] Tests